//
//  Grid.swift
//  GameOfLife
//
//  Created by Bojan Gaspar on 08/06/2020.
//  Copyright © 2020 UltraDev. All rights reserved.
//

import Foundation

public class Grid {

    public var cells = [Cell]()
    public let size: Int // number od elements in one row/column

    public init(size: Int) {
        self.size = size
        setupCells()
    }

    private func setupCells() {
        // create the cells
        for _ in 0..<(size * size) {
            let randomState = arc4random_uniform(4) // every fourth cell will be alive at the begining
            let cell = Cell(state: randomState == 0 ? .alive : .dead)

            //Test glider
//            let cell = Cell(state: [2, 3 + size , 3 + size, 1 + size + size, 2 + size + size, 3 + size + size].contains(index) ? .alive : .dead)
            cells.append(cell)
        }
    }

    public func resetGrid() {
        cells = []
        setupCells()
    }

    public func updateCells(){
        var updatedCells: [Cell] = []

        for (index, cell) in cells.enumerated() {
            let numberOfNeighbours = countLivingNeighbours(index: index)

            switch numberOfNeighbours {
            // Any live cell with two or three live neighbours survives.
            case 2...3 where cell.state == .alive:
                updatedCells.append(cell)

            // Any dead cell with three live neighbours becomes a live cell.
            case 3 where cell.state == .dead:
                let liveCell = Cell(state: .alive)
                updatedCells.append(liveCell)

            // All other live cells die in the next generation. Similarly, all other dead cells stay dead.
            default:
                let deadCell = Cell(state: .dead)
                updatedCells.append(deadCell)
            }
        }
        cells = updatedCells
    }

    private func countLivingNeighbours(index: Int) -> Int {
        var livingNeighbours: [Cell] = []

        addLivingNeighbour(to: &livingNeighbours, with: index - size - 1) // top left
        addLivingNeighbour(to: &livingNeighbours, with: index - size) // top
        addLivingNeighbour(to: &livingNeighbours, with: index - size + 1) // top right
        addLivingNeighbour(to: &livingNeighbours, with: index - 1) // left
        addLivingNeighbour(to: &livingNeighbours, with: index + 1) // right
        addLivingNeighbour(to: &livingNeighbours, with: index + size - 1) // bottom left
        addLivingNeighbour(to: &livingNeighbours, with: index + size) // bottom
        addLivingNeighbour(to: &livingNeighbours, with: index + size + 1) // bottom right

        return livingNeighbours.count
    }

    private func addLivingNeighbour(to livingNeighbours: inout [Cell], with neighbourIndex: Int) {
        if cells.indices.contains(neighbourIndex) && cells[neighbourIndex].state == .alive {
            livingNeighbours.append(cells[neighbourIndex])
        }
    }
}
