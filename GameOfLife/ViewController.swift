//
//  ViewController.swift
//  GameOfLife
//
//  Created by Bojan Gaspar on 08/06/2020.
//  Copyright © 2020 UltraDev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var gridContainerView: UIView!
    @IBOutlet weak var pauseResumeButton: UIButton!
    @IBOutlet weak var restartButton: UIButton!

    var gridView: GridView? = nil
    var isRunning = false

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let cellSize = 2
        let viewWidth = Int(gridContainerView.bounds.width)
        gridView = GridView(width: viewWidth + 1 - viewWidth % cellSize, cellSize: cellSize)
        gridContainerView.addSubview(gridView!)
        gridView?.startPause(isRunning: false)
    }
}

// MARK: - IBActions

extension ViewController {
    @IBAction func pauseResumeAction(_ sender: Any) {
        isRunning = !isRunning
        gridView?.startPause(isRunning: isRunning)
        if isRunning {
            pauseResumeButton.setTitle("Resume", for: .normal)
        } else {
            pauseResumeButton.setTitle("Pause", for: .normal)
        }
    }

    @IBAction func restartAction(_ sender: UIButton) {
        gridView?.resetGrid()
    }
}

