//
//  Cell.swift
//  GameOfLife
//
//  Created by Bojan Gaspar on 08/06/2020.
//  Copyright © 2020 UltraDev. All rights reserved.
//

import Foundation

public enum State {
    case alive
    case dead
}

public struct Cell {
    public var state: State
}
