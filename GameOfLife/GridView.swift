//
//  GridView.swift
//  GameOfLife
//
//  Created by Bojan Gaspar on 08/06/2020.
//  Copyright © 2020 UltraDev. All rights reserved.
//

import UIKit

class GridView: UIView {

    var gridSize: Int = 50
    var cellSize: Int = 5
    var grid: Grid = Grid(size: 50)

    var timer = Timer()

    public convenience init(width: Int, cellSize: Int) {
        let frame = CGRect(x: 0, y: 0, width: width, height: width)
        self.init(frame: frame)
        self.cellSize = cellSize
        self.gridSize = width / cellSize
        self.grid = Grid(size: gridSize)
        self.cellSize = cellSize
    }

    deinit {
        timer.invalidate()
    }

    public override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        context?.saveGState()

        for (index, cell) in grid.cells.enumerated() {
            let (x, y) = (index % gridSize, index / gridSize)
            // With visible grid
//            let rect = CGRect(x: 1 + x * cellSize, y: 1 + y * cellSize, width: cellSize - 1, height: cellSize - 1)

            // Without visible grid
            let rect = CGRect(x: x * cellSize, y: y * cellSize, width: cellSize, height: cellSize)
            let color = cell.state == .alive ? UIColor.customRed.cgColor : UIColor.white.cgColor
            context?.addRect(rect)
            context?.setFillColor(color)
            context?.fill(rect)
        }

        context?.restoreGState()
    }

    public func resetGrid() {
        grid.resetGrid()
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(nextStep), userInfo: nil, repeats: true)
    }

    public func startPause(isRunning: Bool) {
        if isRunning {
            timer.invalidate()
        } else {
            timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(nextStep), userInfo: nil, repeats: true)
        }
    }

    @objc private func nextStep() {
        self.grid.updateCells()
        self.setNeedsDisplay()
    }
}
