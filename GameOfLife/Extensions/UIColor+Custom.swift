//
//  UIColor+Custom.swift
//  GameOfLife
//
//  Created by Bojan Gaspar on 10/06/2020.
//  Copyright © 2020 UltraDev. All rights reserved.
//

import UIKit

extension UIColor {

    public class var customRed: UIColor {
        return UIColor(red: 0.929, green: 0.086, blue: 0.329, alpha: 1.0)
    }
}
